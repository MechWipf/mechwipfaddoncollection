
AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

ENT.WireDebugName = "Fin Controller"

include('shared.lua')

local MODEL = Model("models/jaanus/wiretool/wiretool_siren.mdl")


function ENT:Initialize()
	self.Entity:SetModel( MODEL )
	self.Entity:PhysicsInit( SOLID_VPHYSICS )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )

	self.Inputs = Wire_CreateInputs( self.Entity, { "Efficiency" } )
	
	self.Slaves = {}
	
	self:ShowOutput(0)
	
	self.NextUse = CurTime() + 10;
end

function ENT:TriggerInput(iname, value)
	if (iname == "Efficiency") then
		local Eff = math.Clamp(value,0,100)
		self:ShowOutput( Eff )
		for k,v in pairs(self.Slaves) do
			if v:IsValid() then
				v.Fin2_Ent.efficiency = Eff
			end
		end
	end
end

function ENT:ShowOutput( Eff )
	self:SetOverlayText( "Fin Controller\nEfficiency: " .. Eff )
end

function ENT:Use()
	if self.NextUse < CurTime() then
		self:SendMessage("Highlighting Linked Fins for 5 seconds.")
		for k,v in pairs(self.Slaves) do
			if v:IsValid() then
				local col = Color(0,0,0,0)
				col.r,col.g,col.b,col.a = v:GetColor()
				v:SetColor(0,150,255,255)
				timer.Simple(5,function() v:SetColor(col.r,col.g,col.b,col.a) end)
			end
		end	
		self.NextUse = CurTime() + 5.5
	end
end

function ENT:SendMessage(msg)
	if self.Founder then
		self.Founder:SendLua("GAMEMODE:AddNotify('"..msg.."', NOTIFY_GENERIC, 3)")
	end
end

function ENT:BuildDupeInfo()
	local info = self.BaseClass.BuildDupeInfo(self) or {}
	info.Slaves = {}
	for k,v in pairs(self.Slaves) do
		table.insert(info.Slaves,v:EntIndex())
	end
	return info
end

function ENT:ApplyDupeInfo(ply, ent, info, GetEntByID, GetConstByID)
	self.BaseClass.ApplyDupeInfo(self, ply, ent, info, GetEntByID, GetConstByID)
	self.Slaves = {}
	if (GetEntByID) then
		local Slaves = info.Slaves
		for k,v in pairs(Slaves) do
			local ent = GetEntByID(v)
			ent.Controller = self.Entity
			table.insert(self.Slaves,ent)
		end	
	end
end

