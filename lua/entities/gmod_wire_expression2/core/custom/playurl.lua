--[[
	playURL E2 Extension for Garry's Mod 13
	written by MechWipf, 2013
	Do-What-You-Want-Copyright
--]]
E2Lib.RegisterExtension("playURL", true)

util.AddNetworkString( "e2_soundPlayURL" )

e2PlayURL = {}

local Channels = {}
local MAXCHANNELS = 8

function e2PlayURL.ValidAction( self, action )
	local plr = self.player
	if not plr:IsAdmin() then return false end
	if action == "createStatic" and not plr:IsSuperAdmin() then return false end
	if action == "create" and (Channels[self] and #Channels[self] >= MAXCHANNELS) then return false end
	return true
end

function e2PlayURL.ClearSounds( ent )
	if not Channels[ent] then return nil end

	for _,v in pairs( Channels[ent] ) do
		net.Start( "e2_soundPlayURL" )
		 net.WriteString( "soundURLKill" )
		 net.WriteTable( { ent, v } )
		net.Broadcast()
	end
end


e2function void soundURLCreate( string index, string url, number startPlaying )
	if not e2PlayURL.ValidAction( self, "createStatic" ) then return nil end

	if not Channels[self.entity] then
		Channels[self.entity] = {}
	end

	Channels[self.entity][index] = true

	net.Start( "e2_soundPlayURL" )
	 net.WriteString( "soundURLCreate1" )
	 net.WriteTable( { self.entity , index, url, startPlaying } )
	net.Broadcast()
end


e2function void soundURLCreate( string index, string url, vector pos, number startPlaying )
	if not e2PlayURL.ValidAction( self, "create" ) then return nil end

	if not Channels[self.entity] then
		Channels[self.entity] = {}
	end

	Channels[self.entity][index] = true

	net.Start( "e2_soundPlayURL" )
	 net.WriteString( "soundURLCreate2" )
	 net.WriteTable( { self.entity , index, url, pos, startPlaying } )
	net.Broadcast()
end


e2function void soundURLKill( string index )
	if not Channels[self] then return 0 end
	if not Channels[self][index] then return 0 end

	Channels[self][index]:Stop()
	Channels[self][index] = nil

	if #Channels[self] == 0 then
		Channels[self] = nil
	end

	return 1
end


e2function void soundURLPlay( string index )
	if not Channels[self] then return 0 end
	if not Channels[self][index] then return 0 end

	Channels[self][index]:Play()

	return 1
end


e2function void soundURLStop( string index )
	if not Channels[self] then return 0 end
	if not Channels[self][index] then return 0 end

	Channels[self][index]:Stop()

	return 1
end


e2function void soundURLSetPos( string index, vector pos )
	if not Channels[self] then return 0 end
	if not Channels[self][index] then return 0 end

	Channels[self][index]:SetPos( pos )

	return 1
end


e2function void soundURLPause( string index )
	if not Channels[self] then return 0 end
	if not Channels[self][index] then return 0 end

	Channels[self][index]:Pause()

	return 1
end


e2function void soundURLVolume( string index, number vol )
	if not Channels[self] then return 0 end
	if not Channels[self][index] then return 0 end

	Channels[self][index]:SetVolume( vol )

	return 1
end

registerCallback( "destruct", function(self)
	if not self or not IsValid( self.entity ) then return nil end

	e2PlayURL.ClearSounds( self.entity )
end)
