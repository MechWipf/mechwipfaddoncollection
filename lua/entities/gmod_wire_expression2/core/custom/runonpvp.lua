// Author: B0B

local PvpAlert = {}
local runByPvp = 0
local kill = {}

registerCallback("destruct",function(self)
	PvpAlert[self.entity] = nil
end)

hook.Add( "PlayerDeath", "Exp2PvP", function ( victim, inflictor, killer )
	
	runByPvp = 1
	kill = { victim, inflictor, killer, CurTime() }
	
	for e,_ in pairs(PvpAlert) do
		if IsValid(e) then
			e:Execute()
		else
			PvpAlert[e] = nil
		end
	end
	runByPvp = 0
end)


e2function void runOnPvp(activate)
	if activate ~= 0 then
		PvpAlert[self.entity] = true
	else
		PvpAlert[self.entity] = nil
	end
end

e2function number pvpClk()
	return runByPvp
end

e2function entity pvpVictim()
	if not IsValid(kill[1]) then return nil end
	return kill[1]
end

e2function entity pvpInflictor()
	if not IsValid(kill[2]) then return nil end
	return kill[2]
end

e2function entity pvpKiller()
	if not IsValid(kill[3]) then return nil end
	return kill[3]
end

e2function number pvpLastKill()
	return kill[4]
end

e2function number pvpKill()
	return kill
end