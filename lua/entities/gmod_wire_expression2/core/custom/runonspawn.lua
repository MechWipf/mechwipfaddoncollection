local runOnSpawn = 0
local plySpawn = nil
local chips = {}

registerCallback('destruct', function(self)
	chips[self.entity] = nil
end)

hook.Add('PlayerSpawn', 'E2_PlayerSpawn', function(ply)
	runOnSpawn = 1
	plySpawn = ply

	for e,_ in pairs(chips) do
		if IsValid(e) then
			e:Execute()
		else
			chips[e] = nil
		end
	end

	plySpawn = nil
	runOnSpawn = 0
end)

e2function void runOnSpawn(on)
	if on ~= 0 then
		chips[self.entity] = true
	else
		chips[self.entity] = nil
	end
end

e2function entity spawnPly()
	if not IsValid(plySpawn) then return nil end
	return plySpawn
end

e2function number spawnClk()
	return runOnSpawn
end