
local sbox_E2_FacePosingOn		= CreateConVar( "sbox_E2_FacePosingOn", "1", FCVAR_ARCHIVE )
local sbox_E2_RagdollPosingOn	= CreateConVar( "sbox_E2_RagdollPosingOn", "1", FCVAR_ARCHIVE )
local sbox_E2_PropAnimOn		= CreateConVar( "sbox_E2_PropAnimOn", "1", FCVAR_ARCHIVE )

-- [[  Ragdoll Posing  ]] --

__e2setcost(20)

e2function void bone:boneFreeze(num)
	if !sbox_E2_RagdollPosingOn then return end
	local ent = E2Lib.isValidBone(this)
	if not ent then return end
	if not E2Lib.isOwner(self, ent) then return end
	
	this:EnableMotion(!num)
end

--[[ Not Working sorry :/
e2function void bone:boneSetPos(vector pos,angle ang)
	if !sbox_E2_RagdollPosingOn then return end
	local ent, idx = E2Lib.isValidBone2(this)
	if not ent then return end
	if not E2Lib.isOwner(self, ent) then return end
	
	ent:SetBonePosition(idx,pos,ang)
end
]]--

-- [[ Face Posing ]] --
__e2setcost(2)

local function ValidFaceEnt(ent)
	if ent:IsNPC() then return true end
	if ent:GetClass() == "prop_ragdoll" then return true end
	if ent:GetClass() == "gmod_wire_hologram" then return true end
	return false
end

e2function number entity:faceGetFlexNum()
	if !sbox_E2_FacePosingOn then return 0 end
	if not this then return 0 end
	if not this:IsValid() then return 0 end
	if not ValidFaceEnt(this) then return 0 end

	return this:GetFlexNum() or 0
end

e2function string entity:faceGetFlexName(num)
	if !sbox_E2_FacePosingOn then return "" end
	if not this then return "" end
	if not this:IsValid() then return "" end
	if not ValidFaceEnt(this) then return "" end
	
	return this:GetFlexName(num) or ""
end

__e2setcost(5)

e2function void entity:faceSetFlexWeight(i,num)
	if !sbox_E2_FacePosingOn then return end
	if not this then return end
	if not this:IsValid() then return end
	if not ValidFaceEnt(this) then return end
	if not E2Lib.isOwner(self,this) then return end
	
	local i = math.Clamp(i,0,this:GetFlexNum())
	local num = math.Clamp(num,0,1)
	
	this:SetFlexWeight(i,num)
end

e2function void entity:faceSetFlexScale(num)
	if !sbox_E2_FacePosingOn then return end
	if not this then return end
	if not this:IsValid() then return end
	if not ValidFaceEnt(this) then return end
	if not E2Lib.isOwner(self,this) then return end

	local num = math.Clamp(num,-1,99)
	this:SetFlexScale(num)
end
-- [[ Finger Posing ]] --
--empty
-- [[ Eye Posing ]] --
local function ConvertRelativeToEyesAttachment( Entity, pos )
	-- Convert relative to eye attachment
	local eyeattachment = Entity:LookupAttachment( "eyes" )
	if ( eyeattachment == 0 ) then return end
	local attachment = Entity:GetAttachment( eyeattachment )
	if ( !attachment ) then return end
	local LocalPos, LocalAng = WorldToLocal( pos, Angle(0,0,0), attachment.Pos, attachment.Ang )
	return LocalPos

end

e2function void entity:eyeSetTarget(vector pos)
	if !sbox_E2_FacePosingOn then return end
	if not this then return end
	if not this:IsValid() then return end
	if not this:GetClass() == "prop_ragdoll" then return end
	if not E2Lib.isOwner(self,this) then return end
	
	local LPos = ConvertRelativeToEyesAttachment(this, Vector(pos[1],pos[2],pos[3]))
	if not LPos then return end
	
	this:SetEyeTarget(LPos)
end

--[[ not working ... sorry :/
local function AddBonePosHook(ent)
	if ( !ent || !ent:IsValid() || ent:GetClass() != "prop_ragdoll" ) then return end
	
	local CurFunc = ent.BuildBonePositions
	
	if CurFunc then
		ent.BuildBonePositions = function()
									CurFunc()
									SetBonePosition(self, numbones, numphysbones)
								end
	else
		ent.BuildBonePositions = SetBonePosition
	end
end

hook.Add( "OnEntityCreated", "AddBonePosHook", AddBonePosHook )
]]--