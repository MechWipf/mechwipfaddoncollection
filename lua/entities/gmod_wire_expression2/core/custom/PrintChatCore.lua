/*
	PrintColor E2 CustomCore for GarrysMod13
	Copyright (C) 2013 MechWipf

	Do what you want with it...
*/

local printColor_typeids = {
	n = tostring,
	s = tostring,
	v = function(v) return Color(v[1],v[2],v[3]) end,
	xv4 = function(v) return Color(v[1],v[2],v[3],v[4]) end,
	e = function(e) return validEntity(e) and e:IsPlayer() and e or "" end,
}

local function printColorVarArg(chip, ply, typeids, ...)
	if (not IsValid(ply)) then return; end
	local send_array = { ... }

	for i,tp in ipairs(typeids) do
		if printColor_typeids[tp] then
			send_array[i] = printColor_typeids[tp](send_array[i])
		else
			send_array[i] = ""
		end
	end

	net.Start("wire_expression2_printColor")
		net.WriteEntity(chip)
		net.WriteTable(send_array)
	net.Send(ply)
end

local printColor_types = {
	number = tostring,
	string = tostring,
	Vector = function(v) return Color(v[1],v[2],v[3]) end,
	table = function(tbl)
		for i,v in pairs(tbl) do
			if !isnumber(i) then return "" end
			if !isnumber(v) then return "" end
			if i < 1 or i > 4 then return "" end
		end
		return Color(tbl[1] or 0, tbl[2] or 0,tbl[3] or 0,tbl[4])
	end,
	Player = function(e) return IsValid(e) and e:IsPlayer() and e or "" end,
}

local function printColorArray(chip, ply, arr)
	if (not IsValid(ply)) then return; end

	local send_array = {}

	for i,tp in ipairs_map(arr,type) do
		if printColor_types[tp] then
			send_array[i] = printColor_types[tp](arr[i])
		else
			send_array[i] = ""
		end
	end

	net.Start("wire_expression2_printColor")
		net.WriteEntity(chip)
		net.WriteTable(send_array)
	net.Send(ply)
end

MechCore = {}
function MechCore.PrintCanUse(ply)
	if ply:IsAdmin() then return true end
	return false
end

e2function void entity:hint(string text, duration)
	if not MechCore.PrintCanUse(self.player) then return end
	if not this:IsValid() then return end
	WireLib.AddNotify(this, text, NOTIFY_GENERIC, math.Clamp(duration,0.7,20))
end

e2function void entity:printColor(...)
	if not MechCore.PrintCanUse(self.player) then return end
	if not this then return end
	if not this:IsValid() and plr:IsPlayer() then return end
	printColorVarArg(self.entity, this, typeids, ...)
end

e2function void entity:printColor(array arr)
	if not MechCore.PrintCanUse(self.player) then return end
	if not this then return end
	if not this:IsValid() and this:IsPlayer() then return end
	printColorArray(self.entity, this, arr)
end

e2function void printColorAll(array arr)
	if not MechCore.PrintCanUse(self.player) then return end
	for k,v in pairs(player:GetAll()) do printColorArray(self.entity, v, arr) end
end
