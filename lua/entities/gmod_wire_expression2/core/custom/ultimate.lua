--[[
	Ultimate E2 CustomCore for GarrysMod13
	Copyright (C) 2010-2013 MechWipf

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

E2Lib.RegisterExtension("ultimate", true)
Ultimate = {}

local Clamp = math.Clamp

local function ColorClamp(col1, col2, col3, col4)
	return Clamp(col1, 0, 255), Clamp(col2, 0, 255), Clamp(col3, 0, 255), Clamp(col4, 0, 255)
end

function Ultimate.ValidAction(self, entity, action)
	local plr = self.player
	if not plr:IsSuperAdmin() then return false end
	if action == "create" then return true end
	if not entity:IsValid() then return false end
	return true
end

e2function entity entCreate(string type)
	if not Ultimate.ValidAction(self, nil, "create") then return nil end
	if type == "lua_run" then return nil end
	local Player = self.player
	local ent = ents.Create(type)

	undo.Create("[E2]"..type)
		undo.AddEntity(ent)
		undo.SetPlayer(Player)
	undo.Finish()

	return ent
end

e2function void entity:entWake(number On)
	if not Ultimate.ValidAction(self, this, "standard") then return end
	local phys = this:GetPhysicsObject()
	if (!phys:IsValid()) then return void end
	if (On) then
		phys:Wake()
	else
		phys:Sleep()
	end
	return void
end

e2function void entity:entSetPos(vector pos)
	if not Ultimate.ValidAction(self, this, "standard") then return end
	pos = Vector(pos[1],pos[2],pos[3])
	this:SetPos(pos)
end

e2function void entity:entSpawn()
	if not Ultimate.ValidAction(self, this, "advanced") then return end
	this:Spawn()
end

e2function void entity:entSetAngles(angle ang)
	if not Ultimate.ValidAction(self, this, "standard") then return end
	ang = Angle(ang[1],ang[2],ang[3])
	this:SetAngles(ang)
end

e2function void entity:entSetModel(string model)
	if not Ultimate.ValidAction(self, this, "standard") then return end
	this:SetModel(model)
end

e2function void entity:entActivate()
	if not Ultimate.ValidAction(self, this, "standard") then return end
	this:Activate()
end

e2function void entity:entKillConstraints()
	if not Ultimate.ValidAction(self, this, "standard") then return end
	constraint.ForgetConstraints( this )
end

e2function void entity:entSetColor(vector rv2)
	if not Ultimate.ValidAction(self, this, "standard") then return end

	local _,_,_,alpha = this:GetColor()
	this:SetColor(ColorClamp(rv2[1], rv2[2], rv2[3], alpha))
end

e2function void entity:entSetColor(vector4 rv2)
	if not Ultimate.ValidAction(self, this, "standard") then return end

	this:SetColor(ColorClamp(rv2[1], rv2[2], rv2[3], rv2[4]))
end

e2function void entity:entSetAlpha(rv2)
	if not Ultimate.ValidAction(self, this, "standard") then return end

	local r,g,b = this:GetColor()
	this:SetColor(r, g, b, Clamp(rv2, 0, 255))
end

e2function void entity:entSetMaterial(string material)
	if not Ultimate.ValidAction(self, this, "standard") then return end

	this:SetMaterial(material)
end

e2function void entity:entSetOwner( entity ply )
	if not Ultimate.ValidAction(self, this, "advanced") then return end
	if not ply:IsPlayer() then return end

	this:CPPISetOwner(ply)
end

-- Fire lua wrapper
e2function void entity:fireInput(string cmd, string arg, number int)
	if not Ultimate.ValidAction(self, this, "advanced") then return end
	if string.find(string.lower(arg),"code") and string.find(string.lower(arg),"addoutput") then
		if not Ultimate.ValidAction(self, this, "special") then return end
	end
	this:Fire(cmd,arg,int)
end

-- Fire lua wrapper
e2function void entity:fireInput(string cmd, number arg, number int)
	if not Ultimate.ValidAction(self, this, "advanced") then return end
	if string.find(string.lower(arg),"code") and string.find(string.lower(arg),"addoutput") then
		if not Ultimate.ValidAction(self, this, "special") then return end
	end
	this:Fire(cmd,arg,int)
end

e2function table entity:getKeyValues()
	if not Ultimate.ValidAction(self, this, "advanced") then return end
end

-- SetKeyValue lua wrapper
e2function void entity:setKeyValue(string val, string arg)
	if not Ultimate.ValidAction(self, this, "advanced") then return end
	this:SetKeyValue(val,arg)
end

-- SetKeyValue lua wrapper
e2function void entity:setKeyValue(string val, number arg)
	if not Ultimate.ValidAction(self, this, "advanced") then return end
	this:SetKeyValue(val,arg)
end

-- Freezes or unfreezes an Entity
e2function void entity:entFreeze(number arg)
	if not Ultimate.ValidAction(self, this, "standard") then return end
end

-- Sets the Velocity of an Entity
e2function void entity:entSetVel(vector arg)
	if not Ultimate.ValidAction(self, this, "standard") then return end
	local phys = this
	if not this:IsPlayer() then
		phys = this:GetPhysicsObject()
	end
	phys:SetVelocity(Vector(unpack(arg)))
end

-- Sets the Eye Angle of a Player
e2function void entity:entSetEyeAngles(angle ang)
	if not Ultimate.ValidAction(self, this, "standard") then return end
	if not this:IsPlayer() then return end

	this:SetEyeAngles(Angle(ang[1],ang[2],ang[3]))
end


-- Respawn / Spawn the Player E
e2function void entity:spawnPly()
	if not Ultimate.ValidAction(self, this, "standard") then return end
    if not IsValid(this) and not this:IsPlayer() then return end
    this:Spawn()
end

-- Remove all Weapons from Player E
e2function void entity:stripWep()
	if not Ultimate.ValidAction(self, this, "standard") then return end
    if not IsValid(this) and not this:IsPlayer() then return end
	this:StripWeapons()
end

__e2setcost(5) -- approximation

-- Set the Health of the Player to Number
e2function void entity:setHealth(number)
	if not Ultimate.ValidAction(self, this, "standard") then return end
    if not IsValid(this) then return end
    if not this:IsPlayer() then return end
    this:SetHealth(number)
end

-- Set the Armor of the Player to Number
e2function void entity:setArmor(number)
	if not Ultimate.ValidAction(self, this, "standard") then return end
    if not IsValid(this) then return end
    if not this:IsPlayer() then return end
    this:SetArmor(number)
end

-- Set the Normal Speed of the Player to Number
e2function void entity:setSpeed(number)
	if not Ultimate.ValidAction(self, this, "standard") then return end
    if not IsValid(this) then return end
    if not this:IsPlayer() then return end
    this:SetWalkSpeed(number)
end

-- Reset the Normal Speed of the Player
e2function void entity:setSpeedReset()
	if not Ultimate.ValidAction(self, this, "standard") then return end
    if not IsValid(this) then return end
    if not this:IsPlayer() then return end
    this:SetWalkSpeed(500)
end

-- Set the Shift Speed of the Player to Number
e2function void entity:runSpeed(number)
	if not Ultimate.ValidAction(self, this, "standard") then return end
    if not IsValid(this) then return end
    if not this:IsPlayer() then return end
    this:SetRunSpeed(number)
end

-- Reset the Shift Speed of the Player
e2function void entity:runSpeedReset()
	if not Ultimate.ValidAction(self, this, "standard") then return end
    if not IsValid(this) then return end
    if not this:IsPlayer() then return end
    this:SetRunSpeed(250)
end

-- Get's the Status id of the Player
e2function number entity:statusID()
    if not IsValid(this) then return 0 end
    if not this:IsPlayer() then return 0 end
    return this:UserID()
end

-- Kills the Player
e2function void entity:kill()
	if not Ultimate.ValidAction(self, this, "standard") then return end
    if not IsValid(this) then return end
    if not this:IsPlayer() then return end
    this:Kill()
end

-- Gives the Player the Weapon 'string weapon'\nExample: E:giveWep("weapon_pistol")
e2function void entity:giveWep(string weapon)
	if not Ultimate.ValidAction(self, this, "standard") then return end
    if not IsValid(this) then return end
    if not this:IsPlayer() then return end
    this:Give( weapon )
end
