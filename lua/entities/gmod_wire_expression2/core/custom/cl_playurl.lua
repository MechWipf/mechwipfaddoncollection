--[[
	playURL Extension for Garry's Mod 13
	written by MechWipf, 2013
	Do-What-You-Want-Copyright
--]]
AddCSLuaFile( "cl_playurl.lua" )

if not CLIENT then return nil end

local Channels = {}
local f = {}

function f.soundURLCreate1( self, index, url, startPlaying )
	if Channels[self] and Channels[self][index] then
		Channels[self][index]:Stop()
	end

	sound.PlayURL( url, ( startPlaying == 1 and "sterio" or "sterio noplay" ), function(sndChn)
		if not sndChn then error( "Failed while creating sound. Check your arguments!" ) end
		if not Channels[self] then
			Channels[self] = {}
		end

		Channels[self][index] = sndChn
	end)
	return 1
end

function f.soundURLCreate2( self, index, url, pos, startPlaying )
	if Channels[self] and Channels[self][index] then
		Channels[self][index]:Stop()
	end

	sound.PlayURL( url, ( startPlaying == 1 and "3d mono" or "3d mono noplay" ), function(sndChn)
		if not sndChn then error( "Failed while creating sound. Check your arguments!" ) end
		if not Channels[self] then
			Channels[self] = {}
		end

		Channels[self][index] = sndChn

		sndChn:SetPos( pos )
	end)
end

function f.soundURLKill( self, index )
	if not Channels[self] then return 0 end
	if not Channels[self][index] then return 0 end

	Channels[self][index]:Stop()
	Channels[self][index] = nil

	if #Channels[self] == 0 then
		Channels[self] = nil
	end
end

function f.soundURLPlay( self, index )
	if not Channels[self] then return 0 end
	if not Channels[self][index] then return 0 end

	Channels[self][index]:Play()

	return 1
end

function f.soundURLStop( self, index )
	if not Channels[self] then return 0 end
	if not Channels[self][index] then return 0 end

	Channels[self][index]:Stop()

	return 1
end

function f.soundURLSetPos( self, index, pos )
	if not Channels[self] then return 0 end
	if not Channels[self][index] then return 0 end

	Channels[self][index]:SetPos( pos )

	return 1
end

function f.soundURLPause( self, index )
	if not Channels[self] then return 0 end
	if not Channels[self][index] then return 0 end

	Channels[self][index]:Pause()

	return 1
end

function f.soundURLVolume( self, index, vol )
	if not Channels[self] then return 0 end
	if not Channels[self][index] then return 0 end

	Channels[self][index]:SetVolume( vol )

	return 1
end

net.Receive( "e2_soundPlayURL", function( len, cl )
	local func = net.ReadString()

	if f[func] then
		local args = net.ReadTable()

		f[func]( unpack( args ) )
	else
		error( "Tried to call nil. " .. func )
	end

end)
