/*-------------------------------------------------------------------------------------------------------------------------
	E2 PrintColor Privileges
-------------------------------------------------------------------------------------------------------------------------*/

local PLUGIN = {}
PLUGIN.Title = "PrintColor Privileges"
PLUGIN.Description = "PrintColor Privileges."
PLUGIN.Author = "MechWipf"
PLUGIN.Privileges = { "E2 PrintColor" }

local function OverwritePC()
	if MechCore then
		MechCore.PrintCanUse = function(ply)
			if ply:EV_HasPrivilege("E2 PrintColor") then return true end
			return false
		end
	end
end

if ( !(MechCore) ) then
	timer.Simple( 1, OverwritePC);
else
	OverwritePC();
end

evolve:RegisterPlugin( PLUGIN )