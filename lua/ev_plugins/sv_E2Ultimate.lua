--[[--------------------------------------------------------------------------------------------------------------------------
	Ultimate Privileges
-------------------------------------------------------------------------------------------------------------------------]]--

local PLUGIN = {}
PLUGIN.Title = "Ultimate Privileges"
PLUGIN.Description = "Ultimate settings."
PLUGIN.Author = "MechWipf"
PLUGIN.Privileges = { "Ultimate_standard", "Ultimate_advanced", "Ultimate_create", "Ultimate_special" }

local function OverwriteUltimate()
	if ( Ultimate ) then
		Ultimate.ValidAction=function (self, entity, cmd)
			local ply = self.player
			if ply:EV_IsOwner() then return true end
			if not ply:EV_HasPrivilege( "Ultimate_"..cmd ) then return false end
			if IsValid(entity) and string.lower(entity:GetClass()) == "lua_run" and
				not ply:EV_HasPrivilege( "Ultimate_special" ) then return false end
			if cmd == "create" then return true end
			if not IsValid(entity) then return false end
			return true
		end
	end
end

if ( !(Ultimate) ) then
	timer.Simple( 1, OverwriteUltimate);
else
	OverwriteUltimate();
end

evolve:RegisterPlugin( PLUGIN )