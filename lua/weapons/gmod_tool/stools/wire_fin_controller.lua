
TOOL.Category		= "Wire - Physics"
TOOL.Name			= "Fin Controller"
TOOL.Command		= nil
TOOL.ConfigName		= ""
cleanup.Register( "fin_control" )

// Add Default Language translation (saves adding it to the txt files)
if CLIENT then
	language.Add( "Tool.wire_fin_controller.name", "Wired Fin Controller" )
	language.Add( "Tool.wire_fin_controller.desc", "Links a fin to a wired controller." )
	language.Add( "Tool.wire_fin_controller.0", "Left click Fin to link. Right Click to spawn controller. Reload to remove the link." )
	language.Add( "Tool.wire_fin_controller.1", "Left click Controller to link to." )
	language.Add( "Undone.fin_control", "Undone Fin Controller" )
	language.Add( "Cleanup.fin_control", "Fin Controller" )
	language.Add( "Cleaned.fin_control", "Cleaned up all Fin Controllers" )
	language.Add( "sboxlimit.fin_control", "You've reached the Fin Controller limit!" )
end

if SERVER then
	CreateConVar('sbox_maxfin_control', 10)
end

function TOOL:LeftClick( trace )
	if (!trace.Hit) then return end
	if (SERVER and !util.IsValidPhysicsObject( trace.Entity, trace.PhysicsBone )) then return end
	if CLIENT then return true end
	
	if (self:GetStage() == 0) then
		if (!trace.Entity:IsValid()) then return end
		if not (trace.Entity.Fin2_Ent) then
			self:SendMessage("That is not a Fin",true)
			return true
		end
		local ent = trace.Entity
		self.Target = ent
		self.OC = ent:GetColor()
		ent:SetColor(Color(0,150,255,255))
		self:SetStage(1)
	else
		if not (trace.Entity:GetClass() == "gmod_wire_fin_controller") then
			self:SendMessage("Invalid Fin Controller",true)
			self:SetStage(0)
			local col = self.OC
			self.Target:SetColor(col)
			return true
		end
		local control = trace.Entity
		local fin = self.Target
		if (fin.Controller) then
			if (control != fin.Controller) then
				self:SendMessage("Fin is linked to a different controller, unlink it first.",true)
				local col = self.OC
				fin:SetColor(col)
				self:SetStage(0)
				return true
			end
		end
		if not (table.HasValue(control.Slaves,fin)) then
			table.insert(control.Slaves,fin)
			local col = self.OC
			fin:SetColor(col)
			fin.Controller = control
			self:SetStage(0)
			self:SendMessage("Fin Successfully Linked",false)
		else
			self:SendMessage("Fin is already linked to that controller.",true)
			local col = self.OC
			fin:SetColor(col)
			self:SetStage(0)
		end
	end
	return true
end

function TOOL:RightClick( trace )
	if (!trace.Hit) then return false end
	if CLIENT then return true end
	if !self:GetSWEP():CheckLimit("fin_control") then return false end
	local ply = self:GetOwner()
	local Pos = trace.HitPos
	local Ang = trace.HitNormal:Angle()
	Ang.pitch = Ang.pitch + 90
	local control = MakeWireFinController(ply,Pos,Ang)
	local min = control:OBBMins()
	control:SetPos( trace.HitPos - trace.HitNormal * min.z )
	local const = WireLib.Weld(control, trace.Entity, trace.PhysicsBone, true)
	
	undo.Create("Fin Controller")
		undo.AddEntity( control )
		undo.AddEntity( const )
		undo.SetPlayer( ply )
	undo.Finish()
	ply:AddCleanup( "fin_control", control )
	ply:AddCount("fin_control", control)
		
	return true
end

function TOOL:Reload( trace )
	if (!trace.Hit) then return false end
	if (SERVER and !util.IsValidPhysicsObject( trace.Entity, trace.PhysicsBone )) then return false end
	if CLIENT then return true end
	if not (trace.Entity.Fin2_Ent) then
			self:SendMessage("That is not a Fin",true)
			return true
		end
	if not (trace.Entity.Controller) then
		self:SendMessage("That Fin is not linked to a controller",true)
		return true
	end
	local ent = trace.Entity
	local controller = ent.Controller
	
	if controller then
		for k,v in pairs(controller.Slaves) do
			if v:IsValid() then
				if (v == ent) then
					ent.Controller = nil
					table.remove(controller.Slaves,k)
					self:SendMessage("Fin Successfully Unlinked",false)
					return true
				end
			end
		end
	else
		ent.Controller = nil
	end
end

if SERVER then
	function MakeWireFinController( pl, Pos, Ang )
		local controller = ents.Create("gmod_wire_fin_controller")
		
		controller:SetPos( Pos )
		controller:SetAngles( Ang )
		controller:SetPlayer(pl)
		controller:Spawn()
		
		return controller
	end

	function TOOL:SendMessage(msg,bool)
		if bool then
			self:GetOwner():SendLua("GAMEMODE:AddNotify('"..msg.."', NOTIFY_CLEANUP, 3)")
		else
			self:GetOwner():SendLua("GAMEMODE:AddNotify('"..msg.."', NOTIFY_GENERIC, 3)")
		end
	end
end
