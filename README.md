## MechWipfs Addon Collection

### Content
* Fixes
    + **UnfreezeAll Prevention** by Xandaros

* Evolve Plugins
    + **Physic Settings** by MechWipf
    + **Team Manager** by Mr. Faul
    + **PrintColor Privileges** by MechWipf
    + **Ultimate Privileges** by MechWipf

* Expression2 Extensions
    + **PrintColor** by MechWipf
    + **RagdollPosingExtension** by MechWipf
    + **PlayURL** by MechWipf
    + **RunOnDamage** unknown author fixed by MechWipf
    + **RunOnPVP** by BOB fixed by MechWipf
    + **RunOnSpawn** unknown author fixed by MechWipf
    + **Ultimate Entity Manipulation** by MechWipf
* Tools
    + **Fin Controller** unknown author fixed by MechWipf

### Information
Some Expression2 Extensions are highly unstable or in developement,
expecially Ultimate and PlayURL.  
Use with caution!